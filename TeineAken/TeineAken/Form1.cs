﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeineAken
{
    public partial class Form1 : Form
    {
        static int loendur = 0;
        int number = loendur++;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            (new Form1()).Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.label1.Text = $"mina olen {this.number} aken {loendur} aknast";

            this.listBox1.DataSource = Program.Nimed;

            this.dataGridView1.DataSource = Program.Inimesed;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.Nimed.Add(this.textBox1.Text);
            listBox1.DataSource = null;
            this.listBox1.DataSource = Program.Nimed;
            


        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.Text = $"valitud on {this.listBox1.SelectedItem}";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Program.Nimed.Remove(this.listBox1.SelectedItem.ToString());

            listBox1.DataSource = null;
            this.listBox1.DataSource = Program.Nimed;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Program.Inimesed.Add(
                new Inimene
                {
                    Nimi = this.textBox2.Text,
                    Vanus = int.Parse(this.textBox3.Text),
                    Kinganumber = int.Parse(this.textBox4.Text)

                }
                );
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Program.Inimesed;
        }
    }
}
