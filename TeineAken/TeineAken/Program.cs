﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeineAken
{
    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }
        public int Kinganumber { get; set; }

    }

    static class Program
    {

        public static List<string> Nimed = new List<string> { "Henn", "Ants", "Peeter", "Jaak" };

        public static List<Inimene> Inimesed = new List<Inimene>
        {
            new Inimene {Nimi = "Henn", Vanus = 64, Kinganumber = 45  },
            new Inimene {Nimi = "Ants", Vanus = 40, Kinganumber = 43  },
            new Inimene {Nimi = "Peeter", Vanus = 28, Kinganumber = 42  },
            new Inimene {Nimi = "Jaak", Vanus = 36, Kinganumber = 27  },

        };

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
