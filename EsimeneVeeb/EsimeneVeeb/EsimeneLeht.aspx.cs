﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EsimeneVeeb
{
    public partial class EsimeneLeht : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();
            EmployeeList.DataSource = 
            ne.Employees
                .Select(x => new { x.FirstName, x.LastName })
                .ToList();
            EmployeeList.DataBind();

            int categoryId =
                int.TryParse(Request.Params["kategooria"] 
                ?? Request.Params["kat"]
                ??
                "0", out int i) ? i : 0;

            ProductList.DataSource =
                ne.Products
                .Where(x => categoryId == 0 || categoryId == x.CategoryID)
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.Category.CategoryName })
                .ToList();
            ProductList.DataBind();

            this.CategoryName.Text=
            ne.Categories.Find(categoryId)?.CategoryName ?? "Kõik tooted";

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Request.Params["nimi"] == null)
            this.Label1.Text = $"Tere {this.TextBox1.Text}!";
            else this.Label1.Text = $"Tere {Request.Params["nimi"]}!";
            this.Title = "Req:" + Request.QueryString;
        }

        protected void EmployeeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            EmployeeList.PageIndex = e.NewPageIndex;
            EmployeeList.DataBind();
        }

        protected void ProductList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ProductList.PageIndex = e.NewPageIndex;
            ProductList.DataBind();
        }
    }
}