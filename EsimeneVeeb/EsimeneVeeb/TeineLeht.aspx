﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeineLeht.aspx.cs" Inherits="EsimeneVeeb.TeineLeht" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DataList ID="DataList1" runat="server" DataKeyField="CategoryID" DataSourceID="SqlDataSource1">
                <ItemTemplate>
                    &nbsp;<br />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "./EsimeneLeht.aspx?kat="+Eval("CategoryId") %>' Text='<%# Eval("CategoryName") %>'></asp:HyperLink>
<br />
                </ItemTemplate>
            </asp:DataList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" SelectCommand="SELECT [CategoryID], [CategoryName] FROM [Categories]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
