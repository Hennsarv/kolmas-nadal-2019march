﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnExeption
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("anna kaks arvu: ");



            try
            {
                int a = int.Parse(Console.ReadLine());
                int b = int.Parse(Console.ReadLine());
                Console.WriteLine(a / b);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("ära jaga nulliga");
                throw;
            }
            catch(FormatException e)
            {
                Console.WriteLine("vale formaat");
            }
            catch (Exception e)
            {
                Console.WriteLine("miski läks valesti");
                Console.WriteLine(e.GetType().Name);
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("see mis tuleb igal juhul teha");
            }


        }
    }
}
