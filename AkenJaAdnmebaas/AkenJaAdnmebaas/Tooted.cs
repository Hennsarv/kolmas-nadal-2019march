﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaAdnmebaas
{
    public partial class Tooted : Form
    {
        public int CategoryID = 0;
        public Tooted()
        {
            InitializeComponent();
        }

        private void Tooted_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();
            this.dataGridView1.DataSource = ne.Products
                .Where(x => x.CategoryID == CategoryID || CategoryID == 0)
                .Select(x => new { x.ProductName, x.UnitPrice })
                .ToList();
        }
    }
}
