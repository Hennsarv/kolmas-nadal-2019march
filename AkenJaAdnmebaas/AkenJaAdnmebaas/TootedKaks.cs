﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaAdnmebaas
{
    public partial class TootedKaks : Form
    {
        public TootedKaks()
        {
            InitializeComponent();
        }

        private void TootedKaks_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();
            this.listBox1.DataSource =
                ne.Categories.Select(x => x.CategoryName).ToList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string categoryName = this.listBox1.SelectedItem.ToString();

            NorthwindEntities ne = new NorthwindEntities();
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = 
                ne.Categories
                .Where(x => x.CategoryName == categoryName)
                .SingleOrDefault()?.Products
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.UnitsInStock})
                .ToList();
        }
    }
}
