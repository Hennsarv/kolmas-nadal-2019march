﻿namespace AkenJaAdnmebaas
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.avaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kategooriadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tootedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.töötajadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(69, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Employees";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(69, 96);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Products";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(69, 152);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Categories";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.avaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(675, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // avaToolStripMenuItem
            // 
            this.avaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kategooriadToolStripMenuItem,
            this.tootedToolStripMenuItem,
            this.töötajadToolStripMenuItem});
            this.avaToolStripMenuItem.Name = "avaToolStripMenuItem";
            this.avaToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.avaToolStripMenuItem.Text = "&Ava";
            // 
            // kategooriadToolStripMenuItem
            // 
            this.kategooriadToolStripMenuItem.Name = "kategooriadToolStripMenuItem";
            this.kategooriadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.kategooriadToolStripMenuItem.Text = "&Kategooriad";
            this.kategooriadToolStripMenuItem.Click += new System.EventHandler(this.kategooriadToolStripMenuItem_Click);
            // 
            // tootedToolStripMenuItem
            // 
            this.tootedToolStripMenuItem.Name = "tootedToolStripMenuItem";
            this.tootedToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tootedToolStripMenuItem.Text = "&Tooted";
            this.tootedToolStripMenuItem.Click += new System.EventHandler(this.tootedToolStripMenuItem_Click);
            // 
            // töötajadToolStripMenuItem
            // 
            this.töötajadToolStripMenuItem.Name = "töötajadToolStripMenuItem";
            this.töötajadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.töötajadToolStripMenuItem.Text = "T&öötajad";
            this.töötajadToolStripMenuItem.Click += new System.EventHandler(this.töötajadToolStripMenuItem_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 363);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem avaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kategooriadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tootedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem töötajadToolStripMenuItem;
    }
}