﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaAdnmebaas
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Kategooriad katAken = new Kategooriad();
            katAken.Show();
        }

        private void kategooriadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Kategooriad katAken = new Kategooriad();
            katAken.Show();
        }

        private void töötajadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tooted tooted = new Tooted();
            tooted.Show();
        }

        private void tootedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TootedKaks tootd2 = new TootedKaks();
            tootd2.Show();
        }
    }
}
