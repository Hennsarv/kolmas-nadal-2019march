﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaAdnmebaas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();

            List<string> nimed = new List<string> { "*" };
            nimed.AddRange(
                ne.Employees
                .Where(x => x.Subordinates.Count > 0)
                .Select(x => x.LastName)
                .ToList());
            this.listBox1.DataSource = nimed;

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lastName = this.listBox1.SelectedItem.ToString();
            NorthwindEntities ne = new NorthwindEntities();
            this.dataGridView1.DataSource = ne.Employees
                .Where(x => x.Manager.LastName == lastName || lastName == "*")
                .Select(x => new { x.EmployeeID, x.FirstName, x.LastName, x.HireDate })
                .ToList();
        }
    }
}
