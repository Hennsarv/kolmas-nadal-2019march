﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenJaAdnmebaas
{
    public partial class Kategooriad : Form
    {
        public Kategooriad()
        {
            InitializeComponent();
        }

        private void Kategooriad_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();
            this.dataGridView1.DataSource = ne
                .Categories
                .Select(x => new { x.CategoryID, x.CategoryName })
                .ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tooted tooted = new Tooted();
            try
            {
                if (int.TryParse(this.dataGridView1.SelectedRows[0].Cells[0].Value.ToString(),
                    out int cid))
                {
                    tooted.CategoryID = cid;
                }
            }
            catch { }
            tooted.Show();

            

        }
    }
}
