﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MisOnAken
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.label2.Text = "Ära siis virise";
            this.button2.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.label2.Text = "No tõstame siis sinu palka";
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            this.button2.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            PlayTone(0); // do
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PlayTone(2); // re

        }

        private void button5_Click(object sender, EventArgs e)
        {
            PlayTone(4); // mi

        }

        private void button6_Click(object sender, EventArgs e)
        {
            PlayTone(5); // fa

        }

        private void button7_Click(object sender, EventArgs e)
        {
            PlayTone(7); // sol

        }

        private void button8_Click(object sender, EventArgs e)
        {
            PlayTone(9); // la

        }

        private void button9_Click(object sender, EventArgs e)
        {
            PlayTone(11); // si

        }

        private void button10_Click(object sender, EventArgs e)
        {
            PlayTone(12); // do

        }

        private void button11_Click(object sender, EventArgs e)
        {
            PlayTone(1); // do#

        }

        private void button12_Click(object sender, EventArgs e)
        {
            PlayTone(3); // re#

        }

        private void button13_Click(object sender, EventArgs e)
        {
            PlayTone(6);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            PlayTone(8);

        }

        private void button15_Click(object sender, EventArgs e)
        {
            PlayTone(10);

        }

        void PlayTone(int aste) => Console.Beep((int)(440 * Math.Pow(2, aste / 12.0)),500);
    }
}
