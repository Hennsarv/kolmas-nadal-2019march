﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineDB
{
    class Program
    {
        static void Main(string[] args)
        {
            northwindEntities ne = new northwindEntities();

            ne.Database.Log = Console.WriteLine;

            // ne.Categories.Find(8).CategoryName = "Seafood";
            // ne.SaveChanges();

            // foreach (var x in ne.Products.Include("Category"))
            //     Console.WriteLine($"Toode {x.ProductName} maksab {x.UnitPrice} ja kuulub kategooriasse {x.Category.CategoryName}");

            var q = ne.Products
                .Where(x => x.UnitPrice < 20)
                .OrderBy(x => x.ProductName)
                .Select(x => new { x.ProductName, x.UnitPrice, x.UnitsInStock });

            foreach (var x in q) Console.WriteLine(x);

            // LINQ avaldis
            var q1 = from x in ne.Products
                     where x.UnitPrice < 50
                     orderby x.ProductName
                     select new { x.ProductName, x.UnitPrice, x.UnitsInStock };




        }
    }
}
